#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}

	*size = 0;
	int length = 10;
	char **lines = malloc(length * sizeof(char*));
	char line[20];
	
	while(fgets(line, 20, in)!= NULL){
		char *c = strchr(line, '\n');
        if(c != NULL){
            *c = '\0';
        }
        
		if(length == *size){
			length *= 2;
			lines = realloc(lines, length * sizeof(char*));
		}
		char *s = malloc(20 * sizeof(char));
		strcpy(s, line);
		lines[*size] = s;
		(*size)++;
	}
	return lines;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}